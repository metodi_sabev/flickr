var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */
var paths = {
    'js': './resources/assets/js/',
    'bower': './bower_components/',
}

elixir(function(mix) {
    mix.sass('app.scss', 'public/css/', {
        includePaths: [
            // paths.bower + 'bootstrap/dist/css/bootstrap.css',
        ]
    })
        .copy(paths.bower + 'bootstrap/fonts/**', 'public/fonts/bootstrap')
        .scripts([
            paths.bower + 'jquery/dist/jquery.js',
            paths.bower + 'angular/angular.js',
            paths.bower + 'angular-bootstrap/ui-bootstrap-tpls.js',
            paths.bower + 'angular-resource/angular-resource.js',
            paths.bower + 'angular-sanitize/angular-sanitize.js',
            paths.bower + 'bootstrap/dist/js/bootstrap.js',
            // paths.bower + 'ngInfiniteScroll/build/ng-infinite-scroll.js',
            // paths.js    + 'angular.js',
            // paths.js    + 'controllers/FlickrImages.js',
            // paths.js    + 'services/images.js',
        ], 'public/js/app.js', './')
        .styles([], 'public/css/app.css');
});
