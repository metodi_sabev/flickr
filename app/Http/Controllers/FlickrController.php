<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class FlickrController extends Controller
{
    public function index(Request $request)
    {
    	$data = $request->all();
    	$url = 'https://api.flickr.com/services/feeds/photos_public.gne?format=php_serial';

    	if ($data) {
    		$url .= '&tags=' . $data['search'];
    	}

    	$client = new \GuzzleHttp\Client();
    	$response = $client->request('GET', $url);
    	
    	$flickrinput = $response->getBody()->getContents();
    	$flickrinput = unserialize($flickrinput);
    	
    	return \Response::json($flickrinput);
    }
}
