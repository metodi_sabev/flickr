(function() {
    'use strict';

    angular
        .module('flickrImages')
        .controller('ImageEntry', ImageEntry);

    function ImageEntry(imagesService) {
        var vm = this;

        vm.images = [];
        vm.loading = false;
        vm.search = '';

        var sendRequest = function () {
            vm.loading = true;
            
            return imagesService.getImages(vm.search);
        }

        vm.loadImages = function () {
            sendRequest().success(function(result) {
                vm.images = vm.images.concat(result.items);
                vm.loading = false;
            });
        }

        vm.searchImages = function () {
            sendRequest().success(function(result) {
                vm.images = result.items;
                vm.loading = false;
            });
        }

        vm.searchImages();
    }
})();