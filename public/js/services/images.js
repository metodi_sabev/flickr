(function() {
    'use strict';

    angular
        .module('flickrImages')
        .factory('imagesService', images);

        function images($http)
        {
            function getImages(search) {
                search = search.replace(' ', ',');
                return $http.get('api/v1/images', {params: {search: search}});
            }

            return {
                getImages: getImages,
            };
        }
})();