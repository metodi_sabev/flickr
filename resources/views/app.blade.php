<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="author" content="Metodi Sabev">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="robots" content="index,follow"/>
        <meta name="googlebot" content="index,follow" />

        <title>Flickr API Integration</title>
        <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
    </head>
    <body ng-app="flickrImages" ng-controller="ImageEntry as vm">
        @include('partials._navbar')

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1">Filter</span>
                        <input type="text" class="form-control" placeholder="" ng-model="vm.filter">
                    </div>
                </div>
            </div>

            <div class="row">
                <div ng-repeat="image in vm.images | filter:vm.filter">
                    <div class="col-md-3">
                        
                        <img src="<% image.photo_url %>" class="img-thumbnail"><br/>
                        <a href="<% image.url %>" target="_blank"><% image.title %></a> by <a href="<% image.author_url %>" target="_blank"><% image.author_name %></a>

                        <div ng-bind-html="image.description_raw"></div><br/>

                        <div ng-if="image.tagsa.length > 1">
                            <strong>Tags:</strong> <% image.tagsa.join(', ') %>
                        </div>
                    </div>

                    <div ng-if="($index + 1) % 4 == 0" class="clearfix"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="load-more-container">
                        <a ng-if="!vm.loading" href="javascript: void(0);" ng-click="vm.loadImages()">Load more</a>
                        <img ng-if="vm.loading" src="{{ asset('/images/loading_dots.gif') }}">
                    </div>
                </div>
            </div>
        </div>
        
    </body>

    <script src="{{ asset('/js/app.js') }}"></script>
    <script src="{{ asset('/js/angular.js') }}"></script>
    <script src="{{ asset('/js/controllers/FlickrImages.js') }}"></script>
    <script src="{{ asset('/js/services/images.js') }}"></script>
</html>
