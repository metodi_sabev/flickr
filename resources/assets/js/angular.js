(function() {
    'use strict';

    angular.module('flickrImages', [
        'ngResource',
        'ui.bootstrap',
        'ngSanitize'
    ], function($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
    });
})();